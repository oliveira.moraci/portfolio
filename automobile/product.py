from automobile.interface import *
from automobile.core import *
from automobile.calculation import *
from automobile.utils import *

from datetime import date
import os

PATH_REPORTING = "./automobile/results/result.csv"
HEADERS = ['product', 'orders', 'percentage', 'average', 'minimum', 'subtotal', 'date']


class Product:

    def __init__(self):
        self._product_selection = ""
        self._product_orders_list = ""
        self._product_and_orders = ""
        self._total_product_and_orders_list = []
        self._updated_list = []
        self.product_exists = False
        self._validation = True
        self._addition = 'y'
        self._all_products_total = 0
        self._single_product_total = 0
        self._percentage = ""
        self._average = ""
        self._min_quant = ""
        self._csv_data = []

        while self._validation:
            self.manager()

        self.reporting()

    def manager(self):
        self._product_selection = validation_product_input()

        if self._product_selection != "":
            self.product_exists = product_existence(self._product_selection, self._total_product_and_orders_list)
            if self.product_exists:
                print(f"Product {self._product_selection} found on database.\n")
                self._product_and_orders = create_object(**{"product": self._product_selection})
                try:
                    self._updated_list = product_update(self._total_product_and_orders_list,
                                                        self._product_selection,
                                                        self._product_and_orders
                                                        )
                except NotImplementedError:
                    print("Something went wrong with updating list")
                else:
                    self._total_product_and_orders_list = self._updated_list
                    self.validation_selection()
                    return
            self._product_and_orders = create_object(**{"product": self._product_selection})
            self._total_product_and_orders_list.append(self._product_and_orders)
            self.validation_selection()
            return
        return

    def validation_selection(self):
        self._validation = False
        while not self._validation:
            self._validation = validation_additional_orders(additional_orders())
            if self._validation == 'n':
                self._validation = False
                return

    def calculation(self):
        self._all_products_total = products_sum(self._total_product_and_orders_list, _switch=True)

        for _products_dict in self._total_product_and_orders_list:
            for keys, values in _products_dict.items():
                if keys == "orders":
                    self._product_orders_list = values

            for _items in [{"subtotal": products_sum(self._product_orders_list, _switch=False)},
                           {"percentage": percentage(products_sum(self._product_orders_list, _switch=False),
                                                     self._all_products_total)},
                           {"average": average(products_sum(self._product_orders_list, _switch=False),
                                               self._product_orders_list)},
                           {"minimum": min_quant(self._product_orders_list)},
                           {"date": str(date.today())}]:
                _products_dict.update(_items)

    def reporting(self):
        self.calculation()

        if os.path.exists(PATH_REPORTING):
            os.remove(PATH_REPORTING)

        if create_csv(**{"headers": HEADERS,
                         "data": self._total_product_and_orders_list,
                         "location": PATH_REPORTING}):
            print("Reporting file created successfully. ")
            print("End of process")
            return
