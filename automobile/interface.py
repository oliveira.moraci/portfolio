def order_input():
    return input(f"\nInsert amount or '0' to exit: ")


def order_validation():
    try:
        # TODO Replace 0 for something else
        # TODO Validate 0
        order = int(order_input())
    except:
        ValueError("Only numerical input accepted.")
        return False
    else:
        return order


def create_object(**kwargs):
    _product_and_orders = {"product": "",
                           "orders": "",
                           "percentage": "",
                           "average": "",
                           "minimum": ""
                           }
    _order_list = []

    _product_selection = kwargs.get("product")

    print(f"Adding orders to {_product_selection}")

    _order_selection = order_validation()

    while _order_selection not in [False, 0]:
        _order_list.append(_order_selection)
        _order_selection = order_validation()

    if _order_selection == 0:
        print("\nKey 0 pressed. Exiting data entry...\n")

    _product_and_orders["product"] = _product_selection
    _product_and_orders["orders"] = _order_list
    _product_and_orders["subtotal"] = ""
    _product_and_orders["percentage"] = ""
    _product_and_orders["average"] = ""
    _product_and_orders["minimum"] = ""
    _product_and_orders["date"] = ""

    return _product_and_orders
