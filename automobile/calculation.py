PERCENTAGE = "percentage"
AVERAGE = "average"
MINIMUM = "minimum"
BASE_PERCENTAGE = 100


def from_dict_to_product_orders_list(_product_orders_list):
    _list = []
    for keys, values in _product_orders_list.items():
        _list = values
    return _list


def products_sum(_all_prod_order_list, _switch):
    _total_orders = 0

    if _switch:
        for _orders_dict in _all_prod_order_list:
            for keys, values in _orders_dict.items():
                if keys == "orders":
                    for _orders in values:
                        _total_orders = _total_orders + _orders

    else:
        for _orders in _all_prod_order_list:
            _total_orders = _total_orders + _orders

    return _total_orders


def percentage(_sum, _total):
    return f"{int((_sum / _total) * BASE_PERCENTAGE)}%"


def average(_sum, _product_orders_list):
    return int(_sum / len(_product_orders_list))


def min_quant(_orders):
    _min = _orders[0]

    for items in _orders:
        if _min > items:
            _min = items

    return _min
