SCREEN_OPTIONS = {1: "Family", 2: "Work"}


def product_input():
    for key, value in SCREEN_OPTIONS.items():
        print(f"{key} - {value}")

    return input("\nInsert data: ")


def validation_product_input():
    try:
        _input = int(product_input())
    except:
        print("\nSelection should be numeric and one of the options on the list.\n")
        return False
    else:
        for key, value in SCREEN_OPTIONS.items():
            if _input == key:
                return value
        else:
            print("\nIncorrect parameter."
                  "\nSelection should be numeric and one of the options on the list."
                  "\nPlease try it again.\n")
            return False


def additional_orders():
    _input = input("\nData entry completed.\n"
                   "Would you like to add another product? ('y' or 'n')\n")

    return _input


def validation_additional_orders(_param):
    if _param in ['y', 'n']:
        return _param
    else:
        return False


def product_existence(_product, _total_product_orders_list):
    if _total_product_orders_list:
        for _orders_dicts in _total_product_orders_list:
            for keys, values in _orders_dicts.items():
                if values == _product:
                    return True
        return False
    else:
        return False


def product_update(_current_all_prod_order_list, _product, _single_prod_order_dict):
    _single_prod_order_list = []

    if not _current_all_prod_order_list or not _product or not _single_prod_order_dict:
        return False

    for keys, values in _single_prod_order_dict.items():
        if keys == "orders":
            _single_prod_order_list = values

    for _orders_dict in _current_all_prod_order_list:
        if _orders_dict['product'] == _product:
            _orders_dict['orders'] = _orders_dict['orders'] + _single_prod_order_list
    return _current_all_prod_order_list
