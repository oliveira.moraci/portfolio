import csv


def create_csv(**kwargs):
    _headers = kwargs.get("headers")
    _data = kwargs.get("data")
    _location = kwargs.get("location")

    try:
        with open(_location, 'w') as file:
            wr = csv.DictWriter(file, fieldnames=_headers)
            wr.writeheader()
            wr.writerows(_data)
    except NotImplementedError:
        print("Something went wrong while creating CSV file.")
        return False
    else:
        return True
