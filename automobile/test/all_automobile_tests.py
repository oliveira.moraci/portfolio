from automobile.calculation import *
from automobile.core import *
from automobile.interface import *
from automobile.utils import *
import mock
import unittest


class Calculation(unittest.TestCase):

    def test_products_total(self):
        self.assertEqual(products_sum([{'orders': [1, 2, 3]}, {'orders': [4, 5, 6]}], True), 21)

    def test_percentage(self):
        self.assertEqual(percentage(10, 100), "10%")

    def test_average(self):
        self.assertEqual(average(9, [2, 3, 4]), 3)

    def test_min_quant(self):
        self.assertEqual(min_quant([2, 3, 4]), 2)


class Interface(unittest.TestCase):

    # Also tests order_input output result
    @mock.patch("automobile.interface.order_input", return_value=1)
    def test_order_validation_true(self, _input):
        self.assertTrue(type(order_validation()) == int)

    @mock.patch("automobile.interface.order_input", return_value="hola")
    def test_order_validation_false(self, _input):
        self.assertFalse(type(order_validation()) == int)

    @mock.patch("automobile.interface.order_input")
    def test_order_create_object_true(self, mock_input):
        mock_input.side_effect = [1, 0]
        self.assertTrue(type(create_object(**{"product": "Work"})) == dict)


class Core(unittest.TestCase):

    # Also tests product_input
    @mock.patch("automobile.core.product_input")
    def test_user_validation_true(self, mock_product_input):
        mock_product_input.return_value = 1
        self.assertTrue(type(validation_product_input()) == str)

    @mock.patch("automobile.core.product_input")
    def test_user_validation_false(self, mock_product_input):
        mock_product_input.return_value = 100
        self.assertFalse(type(validation_product_input()) == str)

    @mock.patch("automobile.core.product_input")
    def test_user_validation_false(self, mock_product_input):
        mock_product_input.return_value = 100
        self.assertFalse(type(validation_product_input()) == str)

    def test_additional_orders_true(self):
        self.assertTrue(validation_additional_orders('y'))

    def test_additional_orders_false(self):
        self.assertFalse(validation_additional_orders('a'))

    def test_product_existence_true(self):
        self.assertEqual(product_existence('car', [{'product': 'car'}]), True)

    def test_product_existence_false(self):
        self.assertFalse(product_existence('bicycle', [{'product': 'car'}]))

    def test_product_update(self):
        self.assertEqual(
            product_update([{'product': 'car', 'orders': [1, 2, 3]}], 'car', {'product': 'car', 'orders': [4, 5, 6]}),
            [{'product': 'car', 'orders': [1, 2, 3, 4, 5, 6]}])


class Utils(unittest.TestCase):

    def test_create_csv(self):
        self.assertTrue(create_csv(**{"headers": ["data_1", "data_2"],
                                      "data": [{"data_1": "1"}, {"data_2": "2"}],
                                      "location": "../results/result.csv"}))


if __name__ == '__main__':
    unittest.main()
